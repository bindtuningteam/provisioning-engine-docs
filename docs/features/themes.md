1. Select a theme that you want to apply into the SharePoint.
2. Click Review to check the product pretended to install.
3. Install to open a modal with specific information about the installation.

In the target options information select classic and/or modern options. On  Modern Solution you can choose where it will be deployed Tenant App Catalog or Site Collection.

<p class="alert alert-warning">Classic solution are not available to install on a Modern Site.</p>

These options are only valid for Modern Solutions:

- Tenant Wide - It'll install at the APP Catalog Global the solution.
- Site Collection - It'll install at the APP Catalog of Site Collection the solution. If that doesn't exist, it'll try to create this feature on your SharePoint site.

For more information about <a href="https://support.bindtuning.com/hc/en-us/articles/360027696532" target="_blank">APP Catalog</a> option check the link.

For more information about step 2, follow the <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/modern/bindtuning%20setting/">link</a>. Note that you can always edit those option afterward.

![installingtheme.gif](../image/installingtheme.gif)