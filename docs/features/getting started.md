###  Sign in Process

Sign in with your BindTuning account. If you don't have any products, click on <a href="https://bindtuning.com/account/downloads">**Get Started Now**</a> and fill the form.

![loginbind.png](../image/loginbind.png)

Sign in is available for **Office 365** and **SharePoint 2013/2016/2019**, to switch between both you need to click on the corresponding icon for each SharePoint version.

___
### Office 365 

Installations using the **Provisioning Engine desktop application** can be targeted to both individual site collections, as well as multiple site collections. 

To proceed with the installation on multiple site collections, use the **Tenant Administration** URL to login.

![login.png](../image/login.png)

___
### SharePoint On-Premises

<p class="alert alert-info">To use the Provisioning Engine with SharePoint On-Premises, the SharePoint Server Client Components SDK must be installed. More information on the requirements section.</p>

Installations using the **Provisioning Engine desktop application** can be targeted to both individual site collections, as well as multiple site collections.

![login-admin.png](../image/login-admin.png)

<p class="alert alert-info">To proceed with the installation on <strong>multiple site collections</strong>, the Provisioning Engine desktop application needs to be <strong>installed on your Application server</strong>, in order to retrieve the associated information from the Web Application.</p>

___
After you Sign in on your SharePoint platform you'll see that on your home section of Provisioning Engine, there are some shortcuts that you can use to help you reach the desired tabs.

Currently you can access:

- [Install BindTuning Products](../features/web_parts.md) 
- [Install BindTuning Templates](../features/templates.md)
- [Save site as Template](../features/saveastemplate.md) 

![shortcuts.png](../image/shortcuts.png)