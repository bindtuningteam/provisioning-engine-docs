Provisioning provides an option to **Save site as a template** of the connected SharePoint site or its subsites for future use. Then the saved template will be available on **My Downloads** section in the **Templates** Tab.

<p class="alert alert-warning">This functionality might be available depending on the subscription that is associated with the BindTuning account that is logged in on the Provisioning.</p>

![savetemplate.png](../image/savetemplate.png)

1. (PRE) In order to have everything settled you can check on the **SP Environment Preparation** the steps needed before saving it to make sure everything runs just fine.
2. Choose the website and fill the info needed.
    ![savetemplatemodal.png](../image/savetemplatemodal.png)

3. Review, Accept and then the Provisioning will get all the assets of the site.
    ![reptemplate.png](../image/reptemplate.png)

4. Wait until the Provisioning shows on all items a **green check** or **red cross**. That information will determinate if the Save site as a template has a success or not. 