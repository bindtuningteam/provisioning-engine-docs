The Setting of the Provisioning allows you to switch the API Key in use on Provisioning by adding a Provisioning key at the text area an then click on Activate.

<p class="alert alert-info">It's recommended to restart the Provisioning to avoid any issues when you switch the API KEY. You can click on the BindTuning logo at the left Panel to restart the Provisioning.</p>

It'll also display information about the current key in use.

- Clearing login URLs autocomplete list - when you're at the login page on Office 365 you can clear the dropdown suggestions by clicking on clear.
- Activating Bing **Image of the Day**


![clutch_setup.png](../image/clutch_setup.png)