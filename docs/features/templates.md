1. Select the template pretended to be implemented in the SharePoint.
2. A detailed information tab with a preview of the sites implemented.
3. Install to add additional information.

On the additional information you'll need to select between the options avaiable. 

1. Choose between information about **Creating a new subsite** or **Use Existing site** where the template is going to be applyed.
2. Select the **web parts** to be installed alongside the template. Some of the webparts are required in order for the template to function.
3. Select or not a theme to the template. Contains a **Magic Tool** customization to help with the color scheme based on a logo that you can upload. Note that the templates have specif themes required.
4. Option to **add demo content** on the template. On this option, you will also define where the Modern Solution are installed. 
    - **Tenant Wide** - It'll install at the APP Catalog Global the solution.
    - **Site Collection** - It'll install at the APP Catalog of Site Collection the solution. If that doesn't exist, it'll try to create this feature on your SharePoint site.
5. Final Review of all the parameters selected. Requires accepting terms of service in order to proceed.

For more information about <a href="https://support.bindtuning.com/hc/en-us/articles/360027696532" target="_blank">APP Catalog</a> option check the link.

![installingtemplates.gif](../image/installingtemplates.gif)

<p class="alert alert-warning">Cancelling the installation process might cause issues with further installations so use it with caution.</p>
