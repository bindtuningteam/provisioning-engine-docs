First step is to download the BindTuning Provisioning.

1. Login to your **BindTuning** account;
2. On your home page, select the blue ribbon displaying **Download here our provisioning desktop app**.

![account.png](../image/download-provisioning.png)