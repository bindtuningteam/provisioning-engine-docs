**Activate Custom Scripts** <small>For Installing BindTuning Products</small>

By default, for personal sites and self-service created sites in SharePoint Online, the option of **Custom Script** is disabled.

To have the **Solution** option available in your SharePoint Online, follow the steps below:

<p class="alert alert-warning">If you don't see the Admin tile, you don't have Office 365 administrator permissions in your organization.</p>

1. Log in as a global admin or SharePoint admin;
2. Select the app launcher icon in the upper-left corner and choose **Admin** to open the Office 365 admin center;
3. On the left pane, choose **Admin centers** and then **SharePoint**;
4. Select **Settings**;
5. Under **Custom Script**
6. Click **OK**.

<p class="alert alert-warning">It can take up to 24 hours for the change to take effect.</p>

___

**Deactivate Solutions** <small>For Installing BindTuning Products</small>

Provisioning can't deativate and uninstall manually activated solutions. We recommend deactivating solutions that you will install in the future with the Provisioning.

1. Open the settings menu and click on **Site settings**;

	![sitesettings.png](https://bitbucket.org/repo/647zxpn/images/2259817389-sitesettings.png)

2. Under **Web Designer Galleries**, click on **Solutions**

3. Select an older uploaded solution and click on **Deactivate**;

	![update_1.png](https://bitbucket.org/repo/zpBnoa/images/3483004850-update_1.png)

4. Execute this steps for each solution that you'll install with the Provisioning